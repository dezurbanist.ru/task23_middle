﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_EPAM.Helpers
{
	public static class ListHelper
	{
      public static MvcHtmlString CreateSelectList(this HtmlHelper html, List<string> items, string selectedText = "Выберите значение", object htmlAttributes = null)
      {
         TagBuilder select = new TagBuilder("select");
         TagBuilder selectedOption = new TagBuilder("option");
         selectedOption.SetInnerText(selectedText);
         select.InnerHtml += selectedOption.ToString();
         selectedOption.MergeAttribute("selected", "");
         foreach (string item in items)
         {
            TagBuilder option = new TagBuilder("option");
            option.SetInnerText(item);
            select.InnerHtml += option.ToString();
         }
         select.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
         return new MvcHtmlString(select.ToString());
      }
   }
}