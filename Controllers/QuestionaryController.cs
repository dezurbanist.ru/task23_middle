﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task23_EPAM.Controllers
{
   public class QuestionaryController : Controller
   {
      [HttpGet]
      public ActionResult QuestionaryIndex()
      {
         
         return View();
      }


      [HttpPost]
		public ActionResult QuestionaryIndex(string firstName, string lastName, string sex, string selectCountry)
		{
         TempData["firstName"] = firstName;
         TempData["lastName"] = lastName;
         TempData["sex"] = sex;
         TempData["selectCountry"] = selectCountry;

         return View();
		}
    }
}